<?php
namespace Charm\Middleware;

use Closure;
use Charm\FsRouter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FsRouterMiddleware implements MiddlewareInterface {

    protected Closure $routeHandler;
    protected FsRouter $router;

    /**
     * The route handler callback will receive the resolved file path and an array
     * of variables.
     *
     * @param callable $routeHandler
     * @param FsRouter\Options|null $options
     */
    public function __construct(callable $routeHandler, FsRouter\Options $options=null) {
        $this->routeHandler = $routeHandler(...);
        $this->router = new FsRouter($options);
    }

    /**
     * Undocumented function
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $path = trim(explode("?", $request->getRequestTarget())[0], "/");
        $route = $this->router->route($path);
        if ($route === null) {
            // routing failed, so we pass on this request to the next handler
            return $handler->handle($request);
        }
        return ($this->routeHandler)($route[0], $route[1]);
    }

}
