<?php
namespace Charm\FsRouter;

use Charm\Util\ComposerPath;

class Options extends \Charm\AbstractOptions {

    /**
     * Configuration options for FsRouter
     *
     * @param string $routes_path   Path relative to `composer.json` or absolute path.
     * @param string $extension     File extension when a file is chosen instead of a directory. 
     * @param string $index_file    File name when a route matches a directory.
     * @param string $catchall_file File name when a route does not match a directory or when
     *                              there is no index file.
     */
    public function __construct(
        protected string $routes_path = "routes",
        protected string $extension = ".php",
        protected string $index_file = "index.php",
        protected string $catchall_file = "default.php"
    ) {
        $cwd = getcwd();
        chdir(ComposerPath::get());
        $_routes_path = realpath($routes_path);
        chdir($cwd);
        if ($_routes_path === false) {
            throw new FsRouterError("Configured 'routes_path' (".json_encode($routes_path).") was not found");
        }
        if (!is_dir($_routes_path)) {
            throw new FsRouterError("Configured 'routes_path' (".json_encode($routes_path).") is not a directory");
        }
        $this->routes_path = $_routes_path;
        parent::__construct();
    }
}