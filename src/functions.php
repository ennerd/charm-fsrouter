<?php
namespace Charm\FsRouter;

use Psr\Http\Message\RequestInterface;

/**
 * Function which includes the file outside of the FsRouterMiddleware
 * instance.
 */
function include_file(string $filename, RequestInterface $request) {
    return require($filename);
}
