<?php
namespace Charm;

use Charm\Util\ComposerPath;
use Charm\FsRouter\FsRouterError;
use function Charm\FsRouter\include_file;

class FsRouter {

    const DEFAULT_CONFIG = [
        'routes_path' => null, // defaults to ./routes in the composer.json dir
        'extension' => ".php",
        'index_file' => 'index.php',
        'catchall_file' => 'default.php',
    ];

    public function __construct(FsRouter\Options $options=null) {
        if ($options === null) {
            $options = new FsRouter\Options();
        }

        $this->options = $options;

        // set dynamic defaults
        if (empty($this->options['routes_path'])) {
            $this->options['routes_path'] = ComposerPath::get().'/routes';
        }
    }

    public function route(string $path): ?array {
        $components = explode("/", $path);

        $indexFile = $this->options['index_file'];
        $catchAllFile = $this->options['catchall_file'];
        $extension = $this->options['extension'];
        $routesPath = $this->options['routes_path'];

        $handlerPath = $routesPath;
        $handlerFile = $indexFile;
        $matchOffsets = []; // record which component was exact match, to remove them from vars
        foreach ($components as $i => $component) {
            $matchOffsets[$i] = false;
        }
        foreach ($components as $i => $component) {
            /*if (is_link($finalPath = $handlerPath."/".$component)) {
                throw new \Exception("Path '$finalPath' is a symlink");
            } else*/
            if (is_dir($handlerPath.'/'.$component)) {
                $matchOffsets[$i] = true;
                // enter the directory
                $handlerPath .= '/'.$component;
            } elseif (is_dir($handlerPath.'/_')) {
                // enter the directory
                $handlerPath .= '/_';
            } elseif (file_exists($handlerPath.'/'.$component.$extension)) {
                $matchOffsets[$i] = true;
                // dir doesn't exist: directory catch-all handler
                $handlerFile = $component.$extension;
                // any remaining path components must become vars
                break;
            } elseif (file_exists($handlerPath.'/'.$catchAllFile)) {
                $handlerFile = $catchAllFile;
                break;
            } else {
                // we were unable to resolve the path
                break;
            }
        }

        // resolve 
        while (!is_file($finalPath = $handlerPath."/".$handlerFile)) {
            if ($handlerFile === $indexFile) {
                $handlerFile = $catchAllFile;
            } else {
                // move up the tree to find a catchAllFile or a directory file
                $dirName = basename($handlerPath);
                $handlerPath = dirname($handlerPath);
                if (strpos($handlerPath, $routesPath) !== 0) {
                    // don't allow exiting the routes path
                    return null;
                }
                if (is_file($finalPath = $handlerPath."/".$dirName.$extension)) {
                    break;
                }
                if ($dirName !== '_') {
                    $matchOffsets[$i--] = false;
                }
            }
        }
        $vars = [];
        foreach ($components as $i => $component) {
            if (($matchOffsets[$i] ?? false) === false) {
                $vars[] = $component;
            }
        }
        
        if (!file_exists($finalPath)) {
            // unable to resolve the path
            return null;
        }

        return [$finalPath, $vars];
    }
}
