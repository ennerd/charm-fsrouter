charm/fs-router
===============

A router implementation which translates a path to a filename in the file system
according to a set of rules. Can also be used as a PSR-15 routing middleware.

The string `hello/world` is routed to one of the following paths in approximate
order. The target file names `index.php`, and `default.php` as well as the 
extension `.php` is configurable.

* `routes/hello/world/index.php`
* `routes/hello/world/default.php`
* `routes/hello/world.php`
* `routes/_/world/index.php`
* `routes/_/world/default.php`
* `routes/_/world.php`
* `routes/hello/_/index.php`
* `routes/hello/_/default.php`
* `routes/_/_/index.php`
* `routes/_/_/default.php`
* `routes/_/default.php`
* `routes/default.php`

NOTE! This ordering is approximated. Is resolved from the root and in, 
prioritizing entering directories first. When routing fails, it will exit
directories looking

Creating a router instance
--------------------------

    new FsRouterMiddleware([
        'routes_path' => dirname(__DIR__).'/routes'
    ])

